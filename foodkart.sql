-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 03, 2019 at 06:17 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `foodkart`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(16) NOT NULL,
  `customer_name` varchar(256) NOT NULL,
  `custome_mobile` varchar(256) NOT NULL,
  `customer_email` varchar(256) NOT NULL,
  `invoice_no` varchar(256) NOT NULL,
  `order_total` int(16) DEFAULT NULL,
  `product_name` varchar(256) NOT NULL,
  `product_price` int(16) NOT NULL,
  `qty` int(16) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_on` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `customer_name`, `custome_mobile`, `customer_email`, `invoice_no`, `order_total`, `product_name`, `product_price`, `qty`, `created_on`, `updated_on`) VALUES
(1, 'Ahalya', '9876543223', 'Ahalya@gmail.com', 'QWERT234567u', 2261, '2', 0, 0, '2019-07-31 05:11:15', '2019-07-31 05:11:15'),
(2, 'qwee', '1234321', 'qw@qwe.com', '1232', 232, 'qwer er', 23, 10, '2019-08-02 07:41:51', '2019-08-02 07:41:51'),
(3, 'sdfghjkjhgf', '9867564', 'dfghj@fghj.com', '2345678', 656, 'wertyu rtyui', 54, 13, '2019-08-02 07:54:49', '2019-08-02 07:54:49'),
(4, 'mahendra Babu', 'babumhndr@gmail.com', '8553149795', '34565432d', 198, 'Classic Boiled Egg Sandwich', 99, 2, '2019-08-03 02:33:13', '2019-08-03 02:33:13');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(16) NOT NULL,
  `product_name` varchar(256) NOT NULL,
  `product_price` int(16) NOT NULL,
  `product_desc` text NOT NULL,
  `ratings` int(16) NOT NULL,
  `product_image` text NOT NULL,
  `ceated_on` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_on` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_name`, `product_price`, `product_desc`, `ratings`, `product_image`, `ceated_on`, `updated_on`) VALUES
(1, 'Classic Boiled Egg Sandwich', 99, '<pre>\r\nWe love a good sandwich. And, you can never go wrong with a simple boiled egg and bell peppers mix enveloped in delicious mayo. Well, we\'ve swapped the fatty condiment for an in-house cashew mayo and and added some spring onion and minced green chilli for a flavourful kick. Our version of the classic boiled egg sandwich is a must-try!\r\n\r\n- Bell peppers rich in antioxidants\r\n- Protein-packed eggs\r\n- Tomato dip made from scratch\r\n\r\nAllergen Information: Contains eggs, mushroom, apple cider vinegar, nuts, mustard and gluten.\r\n\r\n*Served cold.</pre>', 4, 'https://cdn-cloudinary.cure.fit/www-curefit-com/image/upload/w_377,ar_1.33,fl_progressive,f_auto,q_auto/dpr_1/image/singles/eat/meals/EAT3943/2.jpg', '2019-07-31 04:01:59', '2019-07-31 04:01:59'),
(2, 'Idli Sambar \r\n\r\n', 69, 'The perfect breakfast plate, this duo is a hit amongst many and we\'ve put a healthy twist on it by infusing the sambar with protein-rich dal and veggies like eggplant, pumpkin and drumsticks. Served with a side of traditional coconut chutney, this meal will keep you feeling full for longer.\r\n\r\n- Protein and fibre rich sambar\r\n- Sweetened with jaggery, no refined sugars\r\n\r\nAllergen Information: Contains nuts and mustard seeds.', 5, 'https://cdn-cloudinary.cure.fit/www-curefit-com/image/upload/w_485,f_auto,ar_485:605,c_fit,q_auto:eco/dpr_1/image/singles/eat/meals/EAT1505/3.jpg', '2019-07-31 04:03:27', '2019-08-02 08:07:08'),
(3, 'The Anti-Oxidant Kick (200ml)', 69, 'A blend of pineapple, coconut water, blueberry, lemon, chia seeds, mint and lemongrass, the Anti-Oxidant kick is a light yet potent blend that\'s rich in antioxidants. If you\'re concerned about the ill effects of pollution on your health, look no further. Nourish your body with this drink as it\'s enriched compounds will fight those radicals.\r\n\r\n- 100% Cold pressed juice\r\n- Electrolyte-rich coconut water\r\n- Berries and seeds packed with antioxidants\r\n\r\nAllergen information: Contains pineapple and chia seeds.\r\n\r\nServing size is 200ml.\r\n', 4, 'https://cdn-cloudinary.cure.fit/www-curefit-com/image/upload/w_377,ar_1.33,fl_progressive,f_auto,q_auto/dpr_1/image/singles/eat/meals/EAT1118/5.jpg', '2019-07-31 04:04:33', '2019-07-31 04:04:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
